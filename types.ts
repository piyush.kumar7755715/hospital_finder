

 interface HospitalRequestBody {
    name:string;
    address:string;
    latitude:number;
    longitude:number;
}

 interface FindHospitalsRequest {
    latitude:number;
    longitude:number;
    radius:number;
}

export type {HospitalRequestBody,FindHospitalsRequest}