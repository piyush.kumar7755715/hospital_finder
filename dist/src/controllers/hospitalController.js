"use strict";
// // src/controllers/hospitalController.js
// const hospitalModel = require('../models/hospital.js');
// const createHospital = async (req, res) => {
//   const { name, address, longitude, latitude } = req.body;
//   try {
//     await hospitalModel.createHospital(name, address, longitude, latitude);
//     res.status(201).json({ message: 'Hospital created successfully' });
//   } catch (error) {
//     res.status(500).json({ message: 'Error creating hospital', error });
//   }
// };
// const getHospitals = async (req, res) => {
//   try {
//     const hospitals = await hospitalModel.getHospitals();
//     res.status(200).json(hospitals);
//   } catch (error) {
//     res.status(500).json({ message: 'Error retrieving hospitals', error });
//   }
// };
// const getHospitalById = async (req, res) => {
//   const { id } = req.params;
//   try {
//     const hospital = await hospitalModel.getHospitalById(id);
//     if (hospital) {
//       res.status(200).json(hospital);
//     } else {
//       res.status(404).json({ message: 'Hospital not found' });
//     }
//   } catch (error) {
//     res.status(500).json({ message: 'Error retrieving hospital', error });
//   }
// };
// const updateHospital = async (req, res) => {
//   const { id } = req.params;
//   const { name, address } = req.body;
//   try {
//     await hospitalModel.updateHospital(id, name, address);
//     res.status(200).json({ message: 'Hospital updated successfully' });
//   } catch (error) {
//     res.status(500).json({ message: 'Error updating hospital', error });
//   }
// };
// const deleteHospital = async (req, res) => {
//   const { id } = req.params;
//   try {
//     await hospitalModel.deleteHospital(id);
//     res.status(200).json({ message: 'Hospital deleted successfully' });
//   } catch (error) {
//     res.status(500).json({ message: 'Error deleting hospital', error });
//   }
// };
// const findHospitalsWithinRadius = async (req, res) => {
//   try {
//     const latitude = req.body.latitude;
//     const longitude = req.body.longitude;
//     const radius  = req.body.radius;
//     const hospitals = await hospitalModel.findHospitalsWithinRadius(longitude, latitude, radius);
//     res.status(200).json(hospitals);
//   } catch (error) {
//     res.status(500).json({ message: 'Error finding hospitals', error });
//   }
// };
// export {
//   createHospital,
//   getHospitals,
//   getHospitalById,
//   updateHospital,
//   deleteHospital,
//   findHospitalsWithinRadius,
// };
