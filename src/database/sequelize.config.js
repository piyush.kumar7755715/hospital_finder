
const path = require('path')
// require('ts-node/register');
require('dotenv').config();
const config = {
  development:{
      database: process.env.DB_NAME || "my_pgdb",
      username: process.env.DB_USER || "postgres",
      password: process.env.DB_PASS || "password",
      host: process.env.DB_HOST || "localhost",
      dialect:'postgres',
  
   }
}


module.exports =  config; 