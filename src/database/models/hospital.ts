import { Model, DataTypes } from "sequelize";
import sequelize from "."; 
interface HospitalAttributes {
  id?: number;
  name: string;
  address: string;
  geom: {
    type: string;
    coordinates: [number, number];
  };
  updatedAt?: Date;
  deletedAt?: Date;
  createdAt?: Date;
}

class HospitalInstance extends Model<HospitalAttributes> implements HospitalAttributes {
  public id!: number | undefined;
  public name!: string;
  public address!: string;
  public geom!: { type: string; coordinates: [number, number]; };
  public updatedAt!: Date ;
  public deletedAt!: Date ;
  public createdAt!: Date ;

}


HospitalInstance.init(
  {
   id: {
     type: DataTypes.INTEGER,
     autoIncrement: true,
     primaryKey: true,
   },
   name: {
     type: DataTypes.STRING,
     allowNull: false,
   },
   address: {
     type: DataTypes.STRING,
     allowNull: false,
   },
   geom: {
     type: DataTypes.GEOMETRY("POINT", 4326),
     allowNull: false,
   },
 },
 {
  sequelize,
  modelName:'Hospital'
 }
)

export default HospitalInstance;
