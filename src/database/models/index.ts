
import { Sequelize } from "sequelize";
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../sequelize.config.js')[env];



const  sequelize = new Sequelize({...config});

export default sequelize;
