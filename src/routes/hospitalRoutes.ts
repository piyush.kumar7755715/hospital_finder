import express from "express";
const router = express.Router();

import HospitalController from "../controllers/hospitalController";


router.post('/hospitals', HospitalController.createHospital);
router.get('/hospitals', HospitalController.getHospitals);
router.get('/hospitals/:id', HospitalController.getHospitalById);
router.put('/hospitals/:id', HospitalController.updateHospital);
router.delete('/hospitals/:id', HospitalController.deleteHospital);
router.get('/hospitals/within-radius', HospitalController.findHospitalsWithinRadius);

export default router;