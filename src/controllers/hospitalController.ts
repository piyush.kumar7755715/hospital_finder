import { Request, Response } from "express";
import HospitalInstance from "../database/models/hospital";
import { Op, literal } from "sequelize";

class HospitalController {
  // async batchCreateHospitals (req: Request, res: Response){
  //   const hospitals = req.body; 
  //   try {
  //     const createdHospitals = await HospitalInstance.bulkCreate(hospitals);
  //     res.status(201).json(createdHospitals);
  //   } catch (error) {
  //     console.error('Error creating hospitals:', error);
  //     res.status(500).json({ message: 'Failed to create hospitals', error });
  //   }
  // }
  async createHospital(req: Request, res: Response) {
    const hospitals = req.body; 
    try {
      const createdHospitals = await HospitalInstance.bulkCreate(hospitals);
      res.status(201).json(createdHospitals);
    } catch (error) {
      console.error('Error creating hospitals:', error);
      res.status(500).json({ message: 'Failed to create hospitals', error });
    }
  }


  async getHospitals(req: Request, res: Response) {
    try {
      const hospitals = await HospitalInstance.findAll();
      res.status(200).json(hospitals);
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }


  async getHospitalById(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const hospital = await HospitalInstance.findByPk(id);
      if (hospital) {
        res.status(200).json(hospital);
      } else {
        res.status(404).json({ error: "Hospital not found" });
      }
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  async updateHospital(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { name, address, geom } = req.body;
      const [updated] = await HospitalInstance.update(
        { name, address, geom },
        { where: { id } }
      );
      if (updated) {
        const updatedHospital = await HospitalInstance.findByPk(id);
        res.status(200).json(updatedHospital);
      } else {
        res.status(404).json({ error: "Hospital not found" });
      }
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  async deleteHospital(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const deleted = await HospitalInstance.destroy({ where: { id } });
      if (deleted) {
        res.status(204).json();
      } else {
        res.status(404).json({ error: "Hospital not found" });
      }
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  async findHospitalsWithinRadius(req: Request, res: Response) {
    try {
      const { lat, lng, radius } = req.query;
      const hospitals = await HospitalInstance.findAll({
        where: literal(
          `ST_DWithin(geom, ST_SetSRID(ST_MakePoint(${lng}, ${lat}), 4326)::geography, ${radius})`
        ),
      });
      res.status(200).json(hospitals);
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }
}

export default new HospitalController();
