import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import sequelize  from './database/models'; // Adjust the path as needed
import hospitalRoutes from './routes/hospitalRoutes'; // Adjust the path as needed

const app = express();
const PORT = process.env.PORT || 3000;

// Middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Routes
app.use('/api', hospitalRoutes);

// Test database connection and sync models
sequelize.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
    return sequelize.sync(); // Ensure the database is synced
  })
  .then(() => {
    console.log('Database synchronized.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

// Start server
app.listen(PORT, () => {
  console.log(`Server is running on postgresasdfsa ${PORT}`);
});

export default app;
